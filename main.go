package main

import (
	"context"
	"database/sql"
	structures2 "github.com/exgamer/go-rest-sdk/pkg/config/structures"
	helpers "github.com/exgamer/go-rest-sdk/pkg/helpers/db/mysql"
	"github.com/exgamer/go-rest-sdk/pkg/modules/j/jStructures"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"log"
	"time"
	_ "time/tzdata"
	"trainer-service-go/app/entityManager"
	"trainer-service-go/app/handler"
	"trainer-service-go/app/repository"
	"trainer-service-go/app/service"
	"trainer-service-go/config"
	structures "trainer-service-go/config/configStruct"
	gormSql "trainer-service-go/database/gorm"
	"trainer-service-go/server"
)

func init() {
	location, err := time.LoadLocation("Asia/Almaty")
	if err != nil {
		logrus.Fatalf("error loading location: %v", err.Error())
	}

	time.Local = location
}

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	appConfig, dbConfig, redisConfig, restConfig, telegramConfig, err := config.InitConfig()
	if err != nil {
		logrus.Fatalf("error database connection: %v", err.Error())
	}

	db, err := gormSql.NewGormSqlDB(dbConfig)
	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}
	defer closeDbConnection(db)

	appData := &structures.AppData{
		AppConfig:      appConfig,
		DbConfig:       dbConfig,
		RedisConfig:    redisConfig,
		RestConfig:     restConfig,
		TelegramConfig: telegramConfig,
		RequestData: &jStructures.RequestData{
			ServiceName: appConfig.Name,
		},
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	manager := entityManager.NewManager(services, restConfig)
	handlers := handler.NewHandler(manager, appData)

	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	srv := new(server.Server)

	if err := srv.Run(appConfig.ServerAddress, handlers.InitRoutes()); err != nil {
		logrus.Fatalf("error occured while running http server: %s", err.Error())
	}

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}
}

func OpenDbConnection(dbConfig *structures2.DbConfig) (*sql.DB, error) {
	// Open up database connection.
	db, err := helpers.OpenMysqlConnection(dbConfig)

	if err != nil {
		log.Fatal(err)
	}

	return db, err
}

func closeDbConnection(db *gorm.DB) {
	if err := db.Close(); err != nil {
		logrus.Errorf("error occured on db connection close: %s", err.Error())
	}
}
