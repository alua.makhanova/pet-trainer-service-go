package handler

import (
	httpResponse "github.com/exgamer/go-rest-sdk/pkg/http"
	"github.com/gin-gonic/gin"
	"net/http"
	"trainer-service-go/app/responses/errorResponse"
	"trainer-service-go/model"
)

func (h *Handler) create(c *gin.Context) {
	var input model.Trainer
	if err := c.BindJSON(&input); err != nil {
		errorResponse.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	trainer, err := h.manager.CreateTrainer(&input, h.appData)
	if err != nil {
		errorResponse.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	httpResponse.Response(c, http.StatusOK, trainer)
}
