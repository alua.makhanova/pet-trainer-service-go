package entityManager

import (
	"trainer-service-go/app/service"
	structures "trainer-service-go/config/configStruct"
)

type Manager struct {
	services   *service.Service
	restConfig *structures.RestConfig
}

func NewManager(services *service.Service, restConfig *structures.RestConfig) *Manager {
	return &Manager{
		services:   services,
		restConfig: restConfig,
	}
}
