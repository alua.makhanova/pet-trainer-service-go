package entityManager

import (
	"github.com/exgamer/go-rest-sdk/pkg/logger"
	structures "trainer-service-go/config/configStruct"
	"trainer-service-go/model"
)

func (m *Manager) CreateTrainer(trainer *model.Trainer, appData *structures.AppData) (*model.Trainer, error) {
	createdTrainer, err := m.services.IFormService.Create(trainer)
	if err != nil {
		logger.LogError(err)
		return nil, err
	}

	err = m.services.ISmsService.SendTelegram(createdTrainer, appData)
	if err != nil {
		logger.LogError(err)
		return nil, err
	}

	return createdTrainer, nil
}
