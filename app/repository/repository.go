package repository

import (
	"github.com/jinzhu/gorm"
	structures "trainer-service-go/config/configStruct"
	"trainer-service-go/model"
)

type TrainerRepo interface {
	Create(trainer *model.Trainer) (*model.Trainer, error)
}

type SmsRepo interface {
	SendTelegram(trainer *model.Trainer, appData *structures.AppData) error
}

type Repository struct {
	TrainerRepo
	SmsRepo
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		TrainerRepo: NewFormRepository(db),
		SmsRepo:     NewSmsRepository(),
	}
}
