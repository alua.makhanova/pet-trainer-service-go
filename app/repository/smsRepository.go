package repository

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"strconv"
	structures "trainer-service-go/config/configStruct"
	"trainer-service-go/model"
)

type SmsRepository struct {
}

func NewSmsRepository() *SmsRepository {
	return &SmsRepository{}
}

func (s *SmsRepository) SendTelegram(trainer *model.Trainer, appData *structures.AppData) error {
	bot, err := tgbotapi.NewBotAPI(appData.TelegramConfig.BotToken)
	if err != nil {
		log.Panic(err)
	}
	var text string

	text = fmt.Sprintf("Зарегистрировался новый тренер\n\n\nID Тренера: %d\nИмя: %s\nТелефон: %s",
		trainer.ID, trainer.Name, trainer.Phone)

	chatId, err := strconv.ParseInt(appData.TelegramConfig.ChatId, 10, 64)
	if err != nil {
		fmt.Println("error:", err)
		return err
	}

	msg := tgbotapi.NewMessage(chatId, text)

	// Отправляем сообщение
	_, err = bot.Send(msg)
	if err != nil {
		log.Panic(err)
		return err
	}
	return nil
}
