package repository

import (
	"github.com/exgamer/go-rest-sdk/pkg/logger"
	"github.com/jinzhu/gorm"
	"time"
	"trainer-service-go/model"
)

type TrainerRepository struct {
	db      *gorm.DB
	timeout time.Duration
}

func NewFormRepository(db *gorm.DB) *TrainerRepository {
	return &TrainerRepository{db: db, timeout: 30}
}

func (c *TrainerRepository) Create(trainer *model.Trainer) (*model.Trainer, error) {
	result := c.db.Create(trainer)
	if result.Error != nil {
		logger.LogError(result.Error)
		return nil, result.Error
	}

	return trainer, nil
}
