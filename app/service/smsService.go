package service

import (
	"trainer-service-go/app/repository"
	structures "trainer-service-go/config/configStruct"
	"trainer-service-go/model"
)

type SmsService struct {
	repo repository.SmsRepo
}

func NewSmsService(repo repository.SmsRepo) *SmsService {
	return &SmsService{repo: repo}
}

func (s *SmsService) SendTelegram(trainer *model.Trainer, appData *structures.AppData) error {
	return s.repo.SendTelegram(trainer, appData)
}
