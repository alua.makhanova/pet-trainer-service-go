package service

import (
	"trainer-service-go/app/repository"
	structures "trainer-service-go/config/configStruct"
	"trainer-service-go/model"
)

type IFormService interface {
	Create(trainer *model.Trainer) (*model.Trainer, error)
}

type ISmsService interface {
	SendTelegram(trainer *model.Trainer, appData *structures.AppData) error
}

type Service struct {
	IFormService
	ISmsService
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		IFormService: NewFormService(repos.TrainerRepo),
		ISmsService:  NewSmsService(repos.SmsRepo),
	}
}
