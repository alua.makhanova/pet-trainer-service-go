package service

import (
	"trainer-service-go/app/repository"
	"trainer-service-go/model"
)

type TrainerService struct {
	repo repository.TrainerRepo
}

func NewFormService(repo repository.TrainerRepo) *TrainerService {
	return &TrainerService{repo: repo}
}

func (f *TrainerService) Create(trainer *model.Trainer) (*model.Trainer, error) {
	return f.repo.Create(trainer)
}
