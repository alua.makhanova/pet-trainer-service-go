FROM alpine:3.12
WORKDIR /opt
COPY .env .
COPY sport-trainer-service-go .

CMD ["/opt/sport-trainer-service-go"]