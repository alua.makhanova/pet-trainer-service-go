package model

import "time"

type Category struct {
	ID        int        `gorm:"primary_key" json:"id"`
	Name      string     `json:"name"`
	Trainers  []Trainer  `gorm:"foreignKey:CategoryID,constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

func (c *Category) TableName() string {
	return "categories"
}
