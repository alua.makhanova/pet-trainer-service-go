package model

import "time"

type Trainer struct {
	ID          int        `gorm:"primary_key" json:"id"`
	ClientID    int        `json:"client_id"`
	CategoryID  int        `json:"category_id"`
	Name        string     `json:"name"`
	Lastname    string     `json:"lastname"`
	Phone       string     `json:"phone"`
	BirthDate   string     `json:"birth_date"`
	Gender      string     `json:"gender"`
	JobTitle    string     `json:"job_title"`
	Tags        string     `json:"tags"`
	Description string     `json:"description"`
	Rating      float32    `json:"rating"`
	VoteCount   int        `json:"vote_count"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   *time.Time `json:"updated_at"`
}

type Pagination struct {
	Limit int    `json:"limit"`
	Page  int    `json:"page"`
	Sort  string `json:"sort"`
}

func (c *Trainer) TableName() string {
	return "trainers"
}
